const Router = require('koa-router');
const router = new Router();

// const { HttpException } = require(`${process.cwd()}/core/http-exception`);
const {PositiveIntegerValidator} = require('../../validators/validator')

router.post('/v1/:id/classic/latest', (ctx, next) => {

    const path = ctx.request.param;
    const query = ctx.request.query;
    const headers = ctx.request.header;
    const body = ctx.request.body;

    const v = new PositiveIntegerValidator();
    v.validate(ctx);

    // if(true) {
    //     const error = new global.errs.ParameterException();
    //     throw error;
    // }
    
    ctx.body = {
        key: 'classic'
    }
})

module.exports = router